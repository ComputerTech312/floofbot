import socket
import sys
import time
import sqlite3
import os
import traceback
import re

def make_weighted_action_response(weights_dict):

    avgsum = sum(x for x in weights_dict.values())
    responses = list(weights_dict.keys())
    weights = [float(x) / avgsum for x in weights_dict.values()]
    return responses, weights


_pet_responses = {
    'purrs metallically': 1,
    'purrs happily': 1,
    ':3': 5,
    '^w^': 3
}
pet_responses, pet_weights = make_weighted_action_response(_pet_responses)


# This Codeblock done by AmeliaFloof <3
_bonk_responses = {
    'yelps loudly': 2,
    'growls angrily': 2,
    'Owie!': 3,
    'Stap!': 3,
}
bonk_responses, bonk_weights = make_weighted_action_response(_bonk_responses)


class furDB:

    def __init__(self, file='fur.db'):
        file_exists = os.path.exists(file)
        self.conn = sqlite3.connect(file)
        self.c = self.conn.cursor()
        if not file_exists:
            self.create_schema()


    def __del__(self):
        self.conn.close()

    def create_schema(self):

        self.c.execute('''CREATE TABLE furs
                     (nick text NOT NULL, desc text)''')

        self.c.execute('''CREATE INDEX furs_nicks
                          ON furs(nick)''')

        self.conn.commit()


    def update_desc(self, nick, desc):

        nick_exists = list(self.c.execute(f'''SELECT (nick) FROM furs WHERE nick=?
        ''', (nick,)))
        if not nick_exists:
            self.c.execute(f'''INSERT INTO furs (nick, desc) VALUES (?, ?)
            ''', (nick, desc,))
        else:
            self.c.execute(f'''UPDATE furs SET desc=?
            WHERE nick=?
                        ''', (desc, nick,))

        self.conn.commit()

    def get_desc(self, nick):
        desc = list(self.c.execute(f'''SELECT (desc) FROM furs WHERE nick = ?
                ''', (nick,)))
        if desc:
            return desc[0][0].strip()
        return None

class IRC:
    irc = socket.socket()

    def __init__(self):
        # Define the socket
        # self.irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._buffer = b""

    def disconnect(self):
        self.irc.shutdown(socket.SHUT_RDWR)
        self.irc.close()

    def send(self, channel, msg):
        # Transfer data
        #self.irc.send(bytes("PRIVMSG " + channel + " " + msg + "\n", "UTF-8"))
        pass

    def _send(self, ch, data):

        # and the amount you can stick in a message is approximately 512 - sizeof("\r\n") - sizeof("PRIVMSG ") - sizeof(" :") - sizeof(nick+user+host+"!@") - sizeof(random other crap supported by the IRCd like identify-msg and statusmsg) - sizeof(target)

        msgs = data.split('\n')
        for msg in msgs:
            chunks = [msg[i:i + 350] for i in range(0, len(msg), 350)]
            for chunk in chunks:
                self.irc.send(bytes("PRIVMSG {} :{}\n".format(ch, chunk), "UTF-8"))

    def pm(self, user, msg):
        self._send(user.strip('\r\n'), msg)

    def messageChannel(self, channel, msg):
        self._send(channel, msg)

    def connect(self, server, port, channel, botnick, botpass, botnickpass):
        # Connect to the server
        print("Connecting to: " + server)
        # try:
        #     self.disconnect()
        # except:
        #     pass
        self.irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.irc.connect((server, port))

        # Perform user authentication
        self.irc.send(bytes("USER " + botnick + " " + botnick + " " + botnick + " :python\n", "UTF-8"))
        self.irc.send(bytes("NICK " + botnick + "\n", "UTF-8"))
        self.irc.send(bytes("NICKSERV IDENTIFY " + botnickpass + " " + botpass + "\n", "UTF-8"))
        time.sleep(5)

        # join the channel
        self.irc.send(bytes("JOIN " + channel + "\n", "UTF-8"))

    def get_lines(self):
        resp  = self._buffer + self.irc.recv(2048)
        lines = resp.split(b"\n")
            
        self._buffer = lines.pop(-1)

        return lines

    def yield_lines(self):
        while True:
            lines = self.get_lines()
            for line in lines:
                yield line


db = furDB()


if True:
    import os
    import random
    import configparser

    cfg = configparser.ConfigParser()
    cfg.read('config.ini')

    ## IRC Config
    server = cfg.get('connection', 'server')  # Provide a valid server IP/Hostname
    port = int(cfg.get('connection', 'port'))
    channel = cfg.get('connection', 'channel')

    botnick = cfg.get('connection', 'botnick')
    botnickpass = cfg.get('connection', 'botnickpass')
    botpass = cfg.get('connection', 'botpass')
    irc = IRC()

    available_verbs = ('who', 'desc', 'help', 'append')  # '!counter !del  !img  !link !notify !ping !source  '
    random_endings = ('OwO', 'uwu', ':p', ':3c', '^w^')
    bad_endings = ('>.>', '-.-', ':s')

    def help_commands():
        lines = []
        for key in available_verbs:
            line = f"!{key}: "
            line += cfg.get('helptext', key, fallback='<no description>')
            lines.append(line)
        return lines

    if cfg.has_option('behavior', 'botdesc'):
        db.update_desc(botnick, cfg.get('behavior', 'botdesc'))

    while True:
        print("CONNECTING...")
        irc.connect(server, port, channel, botnick, botpass, botnickpass)

        for line in irc.yield_lines():
            
            try:
                _content_raw = line.replace(b"\r", b'').decode("utf8").split(" ")

            except UnicodeDecodeError:
                print("Invalid unicode content received? : \n", line)
                continue
                
            try:

                if _content_raw[0] == "PING":                    
                    irc.irc.send(bytes('PONG ' + _content_raw[1] + '\r\n', 'utf-8'))

                    continue

                _from = _content_raw[0]
                _from = _from.split('!')[0][1:]
                _type = _content_raw[1]
                _to = _content_raw[2]
                _content = ' '.join(_content_raw[3:])[1:]

                print('from', _from)
                print('type', _type)
                print('to', _to)
                print('content', _content)

                if _type == "QUIT" and "ERROR :Closing Link:" in _content:
                    break

                if _type == ":Closing":
                    break

                if (_type == "PRIVMSG" or _type == "ACTION") and _to == channel and f"pets {botnick}" in _content:
                    # the bot is pet!
                    msg = random.choices(population=pet_responses, weights=pet_weights, k=1)[0]
                    irc.messageChannel(channel, msg)
                    continue

                if (_type == "PRIVMSG" or _type == "ACTION") and _to == channel and f"bonks {botnick}" in _content:
                    #Bonky Bonky!
                    msg = random.choices(population=bonk_responses, weights=bonk_weights, k=1)[0]
                    irc.messageChannel(channel, msg)
                    continue
                
                descmatch = re.match(r".*describe\s(\S+)\s*", _content)
                if _type == "PRIVMSG" and _to == channel and botnick in _content and descmatch:
                    # the bot is asked publicly about someone's description
                    requested_nick = descmatch.groups()[0]
                    if requested_nick == 'yourself':
                        requested_nick = botnick
                    desc = db.get_desc(requested_nick)
                    if desc:
                        msg = f"{requested_nick} is: {desc}"
                    else:
                        msg = f"Could not find any description for {requested_nick} " + random.choice(bad_endings)
                    irc.messageChannel(channel, msg)
                    continue


                if _type == "PRIVMSG" and _to == botnick:
                    # valid PM received
                    if _content.startswith('!help'):

                        msg = cfg.get('helptext', 'helpdesc', fallback='<No greeting defined :s >') + random.choice(random_endings) + \
                               'available commands: \n' + '\n'.join(help_commands())
                    elif _content.startswith('!who '):

                        furry = (_content+" ").split(' ')[1].strip()

                        desc = db.get_desc(furry)
                        if desc:
                            msg = f"{furry} is: {desc}"
                        else:
                            msg = f"Could not find any description for {furry} " + random.choice(bad_endings)
                    elif _content.startswith('!desc '):
                        desc = ' '.join(_content.split(' ')[1:])
                        last_desc = db.get_desc(_from)
                        db.update_desc(_from, desc)
                        msg = f'Your description has been updated! {random.choice(random_endings)} ~'
                        if last_desc:
                            msg += f'(Your last description was [{last_desc}])'
                    elif _content.startswith('!append '):
                        last_desc = db.get_desc(_from) or ''
                        parts = _content.split(' ')
                        if len(parts) > 2 and parts[1] == '-nospace':
                            new_part = ' '.join(parts[2:])
                        else:
                            new_part = ' ' + ' '.join(parts[1:])

                        desc = last_desc + new_part

                        db.update_desc(_from, desc)
                        msg = f'Your description has been updated! (total character count: {len(desc)}) {random.choice(random_endings)} ~'

                    else:
                        msg = f"Sorry, I don't understand that! {random.choice(bad_endings)} ; " + \
                              'available commands: \n' + '\n'.join(help_commands())

                    print("SENDING", _from, msg)

                    irc.pm(_from, msg)


            except Exception as e:
                print('on noes :< !!', e)
                print(_content_raw)
                print(traceback.format_exc())
                continue


        irc.disconnect()

